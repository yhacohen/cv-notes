%!TEX root = cv-notes.tex
\chapter{Clustering}

This chapter covers the basics of clustering and image segmentation methods.

We will start with Markov Random Fields (MRF) and Graph-Cuts.

Then, we will review several clustering algorithm:
\begin{itemize}
\item K-Means
\item Gaussian Mixture via EM
\item Normalized Cuts (N-Cuts)
\item Ratio Cuts
\end{itemize}

We will show that N-Cuts and Ratio Cuts are special cases of K-Means. Furthermore, we will show that K-Means is a special case of Gaussian Mixture via EM.

\section{Markov Random Fields (MRF)}

\subsection{Probability Reminder}

$y=\{y_{1},..,y_{q}\}$, $z=\{z_{1},..,z_{p}\}$ are two random variables.\\
$p(y,z)\in [0,1]^{q\times p}$\\
$p(y|z)=\frac{p(y,z)}{p(z)}$\\
$p(y_{i}|z_{j})=\frac{p(y_{i},z_{j})}{p(z_{j})}$\\
$p(z_{j})=\sum_{i=1}^{q}p(y_{i},z_{j}) \Rightarrow \sum_i p(y_{i}|z_{j})=1$

If $y$ and $z$ are independent $\Leftrightarrow p(y|z)=p(y) \Leftrightarrow p(y,z)=p(y)p(z)$. In this case the matrix is of degree 1 and is given by a cross product of the two random vectors.

$y$ and $z$ are independent given $h \Leftrightarrow p(y|z,h)=p(y|h) \Leftrightarrow(p(y,z|h)=p(y|h)p(z|h)$ 

\subsection{MRF}

MRF models a system with many interacting variables, where each variable can take a value from a discrete set of labels. In the context of clustering, each cluster is represented by a label.
We aim to find a setting of labels to variables that brings the system to an optimal state.
MRF is a framework for setting up problems of this kind in an abstract manner.

$x_{1},...,x_{n}$ are observed points (samples).
$w_{1},...,w_{n} \in [k]$ are the labels (disparities in stereo)

We aim to find an optimal $w$ such that given the observed points $x_{1},...,x_{n}$:
\begin{equation*}
\begin{aligned}
w=
& \underset{w_{1},...,w_{n}}{\text{argmax}}
& & p(w_{1},...,w_{n}|x_{1},...,x_{n}) \\
\end{aligned}
\end{equation*}
This kind of problem is called ``Max A Posteriori'' (MAP).

In general this is an NP-hard problem, and therefore the exact optimal solution cannot be achieved in polynomial time.

Using Bayes Rule:
$P(w|x)=\frac{p(x|w)p(w)}{p(x)}$
We can pose the question as ``How likely is a set of labels given the observed points?'':
\begin{equation*}
\begin{aligned}
w=
& \underset{w_{1},...,w_{n}}{\text{argmax}}
& & p(x|w)p(w) \\
\end{aligned}
\end{equation*}
(Note that the maxima does not depend on $p(x)$).

This is still an NP-hard problem (complexity of search space is still $k^{n}$).
But using this formulation we can use the observed points.
$p(w)$ represents the interactions between variables and $p(x)$ represents the data.

\textbf{Assumption 1}:
We assume that given a fixed value of $w_{i}$, $x_{i}$ is independent of all other variables:
$p(x_{1},...,x_{n}|w_{1},...,w_{n})=\prod_{i=1}^{n}P(x_{i}|w_{i})$

\textbf{Assumption 2}:
The interaction among the state variables is limited to small neighborhoods (i.e. small subsets $N_{i} \subset [n]$).\\
Then:
$w_{i}\perp w_{[n]\backslash N_{i}} | w_{N_{i}} \Rightarrow P(w_{i} | w_{[n] \backslash i})=p(w_{i}|w_{N_{i}})$

\textbf{Implication:}
$p(w)=\prod_{i=1}^{n}p(w_{i}|w_{N_{i}})$
\begin{proof}
$p(w)=p(w_{1}|w_{2},...,w_{n})p(w_{2},...,w_{n})=p(w_{1}|w_{N_{1}})p(w_{2}|w_{3},...,w_{n})p(w_{3},...,w_{n})$ and continue recursively.
\end{proof}
\textbf{Taken together:}
\begin{equation*}
\begin{aligned}
w=
& \underset{w}{\text{argmax}}
& & \prod_{i=1}^{n}p(x_{i}|w_{i}) \prod_{i=1}^{n}p(w_{i}|w_{N_{i}}) \\
\end{aligned}
\end{equation*}

\textbf{Important:} To be an MRF, the model must obey the Markov property:
$p(w_{i}|w_{[n]\backslash i})=p(w_{i}|w_{N_{i}})$

It is more convenient to represent the energy as a sum of terms. To achieve that, we can take $\ln$:
\begin{equation*}
\begin{aligned}
w=
& \underset{w}{\text{argmax}}
& & \sum_{i=1}^{n}\ln p(x_{i}|w_{i}) + \sum_{i=1}^{n}\ln p(w_{i}|w_{N_{i}}) \\
\end{aligned}
\end{equation*}

\textbf{Define:}
$u_{i}(w_{i})=-\ln p(x_{i}|w_{i)}$ and $\psi_{i}(w_{C_{i}})=-\ln p(w_{i} | w_{N_{i}})$ where $C_{i}=N_{i}\cup {i}$ and rewrite the optimization problem as:
\begin{equation*}
\begin{aligned}
w=
& \underset{w}{\text{argmin}}
& & \sum_{i=1}^{n} u_{i}(w_{i}) + \sum_{i=1}^{n} \psi_{i} (w_{C_{i}}) \\
\end{aligned}
\end{equation*}

\subsubsection{Markov Chain (first-order Markov model)}

Each variables depends only on the previous variables:
$p(w_{q} | w_{1},...,w_{q-1}=p(w_{q}|w_{q-1})$

\textbf{Implication:}
$p(w_{1},...,w_{n})=p(w_{1})\prod_{i=2}^{n}p(w_{i}|w_{i-1})$

\begin{equation*}
\begin{aligned}
w=
& \underset{w}{\text{argmin}}
& & \sum_{i=1}^{n} u_{i}(w_{i}) + \sum_{i=2}^{n} \psi_{i} (w_i,w_{i-1}) \\
\end{aligned}
\end{equation*}
while assuming that $p(w_{1})$ is uniform.
This has a polynomial-time solution, but the objective is weaker than the grid model where neighbors may be connected along both axes.

This can be solved in polynomial time, $O(nk^{2})$, using dynamic programming (TBD: see presentation).

\subsection{Markov Grid}

This is a very natural model for images, where each pixel interacts with its four neighbors, in two dimensions.
Generally, this is an NP-hard problem to solve, but special cases can be solved in a polynomial time, such as if $w_{i}\in\{0,1\}$:
\begin{equation}
E(w)=\sum_{i=1}^{n}u_{i}(w_{i}) + \sum_{i < j} \psi_{i,j}(w_{i},w_{j})
\end{equation}

For simplicity, we will demonstrate this on an image with only 2 pixels $i$ and $j$:
\begin{equation}
E(w)=u_{i}(w_{i}) + u_{j}(w_{j}) + \psi_{i,j}(w_{i},w_{j})
\end{equation}

TBD: See class notes for graph illustration.

We construct a graph for which we will find a min-cut.
We define a node for each point (pixel), and two helper nodes $s$ and $t$. Each node is connected to its neighbors. $s$ is connected to all nodes (except $t$) and all other nodes (except $s$) are connected to $t$.
For each edge we define capacity $c \geq 0$ such that every cut in the graph is a possible solution, and the minimal cut (maximum flow) is the optimal solution.
If the $i$-th node is connected to the source in the solution we set $w_{i}=1$.

We must have some assumptions on $\psi$: Since $c_{i}>0$: \\
$\underbrace{c(s,i)+c(s,j)}_{E(0,0)}  +  \underbrace{c(i,t)+c(j,t)}_{E(1,1)} \leq \underbrace{c(i,j)+c(s,j)+c(i,t)}_{E(1,0)}+\underbrace{c(j,i)+c(s,i)+c(j,t)}_{E_{0,1}}$\\
$\Rightarrow  E(0,0)+E(1,1)\leq E(1,0)+E(0,1)$

Now we can substitute the capacities with $u$ and $\psi$:

$E(0,0) = u_{i}(0)+\underbrace{u_{j}(0)+\psi(0,0)}_{c(s,j)}$ (we could add $\psi$ to the other variables instead).

$E(1,1) = u_{i}(1)+\underbrace{u_{j}(1)+\psi(1,1)}_{c(j,t)}$

$E(0,1)=\underbrace{c(j,i)}_{u_{i}(0)}+\underbrace{c(s,i)}_{u_{j}(1)+\psi(1,1)}+\underbrace{c(j,t)}_{\psi(0,1)-\psi(1,1)}$

Re-parameterization:

We can $\delta$ to all solutions without changing the optimal value of $w$:
$E'(1,0)=(c(s,j)+\delta)+(c(i,j)-\delta) + (c(i,t)+\delta) = E(1,0)+\delta$.
Thus we can satisfy the sub-modularity condition, which requires that the sum of the elements on the diagonal will be lower than the some of all other values. If this condition would not have been hold, the min-cut is not the solution, however in computer-vision applications, $\psi(i,i)$ is usually zero and therefore the sub-modularity condition is satisfied.

Note that if $\psi(\alpha,\beta)=f(\alpha-\beta)$ with a convex $f$ there is a polynomial-time solution, but the smoothness constraint is too strong.

\subsubsection{$\alpha$-expansion algorithm}

In each iteration: choose a label $\alpha\in[k]$ and each pixel decides whether to keep its current label or switch to $\alpha$. This is a series of binary problems that each can be solved using the method that is described above.
This strategy requires that $\psi(\alpha,\beta)$ will be a metric (zero for equal points, symmetry, positivity and triangle inequality). TBD: The mapping to the binary problem is not trivial but appears in the lecture presentation.

%%%%%%%%%% EM and K-Means

\section{K-Means and EM}

\subsection{K-Means}

Let's assume that the data points $\{x_{1},...,x_{m}\}$, $x_{i}\in \mathbb{R}^{n}$ can be clustered to $k$ clusters, and let $c_{1},...,c_{k}$ be the unknown class centers.
\textbf{Goal:} We aim to find labels $y_{1},...,y_{m}$ such that $y_{i}=j$ if $x_{i}$ belongs to the cluster whose center is $c_{i}$.
We require that each point belongs to exactly one cluster.

Declare $W_{i,j}=1[y_{i=j}]$ (i.e. $W_{i,j}=1$ if $y_{i}=j$).
Clearly, $W_{ij}={0,1}$ s.t. $\sum_{j=1}^{k}w_{i,j}=1$ uniquely defines label set $\{y_{i}\}_{i=1}^{m}$ and vice-versa.
In k-means, we define an objective function as follows:
\begin{equation*}
\begin{aligned}
& \underset{W,c}{\text{argmin}}
& & \frac{1}{2}\sum_{c=1}^{m}\sum_{j=1}^{k}W_{i,j}\|x_{i}-c_{i}\|^{2} \\
& \text{subject to}
& & W_{i,j}\in \{0,1\}, \\
&&& W \geq  0.
\end{aligned}
\end{equation*}

\begin{algorithm}
  \label{alg:kmeans}
  \caption{K-Means Algorithm}
  \begin{algorithmic}
    \State $W_{i,j}^{0}$ holds initial clustering assignment
    \For  {$t=0,1,...$} 
    \State        $\{c_{1}{(t+1)},...,c_{k}^{t+1}\}=\underset{c_{1},...,c_{k}}{\text{argmin}}\frac{1}{2}\sum_{i,j}W_{i,j}^{(t)}\|x_{i}-c_{j}\|^{2}$ 
    \State        $W_{i,j}^{(t+1)}=1[j=\underset{j}{\text{argmin}}\|x_{i}-c_{j}\|^{2}$ 
    \EndFor 
  \end{algorithmic}
\end{algorithm}

Algorithm \ref{alg:kmeans} approximately minimizes this objective function. In step 2.1 we find $c$ that minimizes the difference w.r.t. to the fixed W:

\begin{align}
\begin{aligned}
0&=\frac{\partial}{\partial c_{j} } \left[ \frac{1}{2}\sum_{i,j}W_{i,j}^{(t)}\|x_{i}-c_{j}\| \right]\\
&=\frac{1}{2}\sum_{i}W_{i,j}^{(t)}(c_{j}-x_{i})\\
&\Rightarrow c_{j}=\frac{1}{\sum_{i}W_{i,j}^{(t)}}\cdot\sum^{m}_{i=1}W_{i,j}^{(t)}x_{i}\\
\end{aligned}
\end{align}

Note that $\sum_{i}W_{i,j}$ is the number of points in cluster $j$ and $\sum_{i}w_{i,j}x_{i}=\sum_{i}1[y_{i=j}]x_{i}=$ sum of points over cluster $j$ (this is a vector). Hence, $c_{j}$ is the mean of cluster $j$.





\subsection{Expectation Maximization (EM) for i.i.d. data}
Now we will see a more general algorithm of which k-means is a special case.

Input data: $D=\{x_{1},...,x_{m}\}$ where $x_{i} \in \mathbb{R}^{n}$

Let $\theta$ be a vector of parameters that describes a possible clustering.
Assuming that $x_{i}$-s are independent given $\theta$, the probability of each clustering is then given by
\begin{equation}
p(D|\theta)=\prod_{i=1}^{m}(x_{i}|\theta)
\end{equation}
Now, we define a helper random variable $y$. According to \emph{the law of total probability}
\begin{equation}
p(D|\theta)=\prod_{i=1}^{m}p(x_{i}|\theta)=\prod_{i=1}^{m}\sum_{j}^{m} p(x_{i},y_{i}=j|\theta)
\end{equation}

By taking $\log$ we get:
\begin{equation}
L(\theta):=\log p(D|\theta)=\sum_{i=1}^{m}\log\left( \sum_{j=1}^{k} p(x_{i},y_{i}=j|\theta) \right)
\end{equation}
Let $W \in \mathbb{R}^{m \times k}$ subject to: $\sum_{j}{W_{i,j}}=1$.
Since $a=\frac{b \cdot a}{b}$ we can write:
\begin{equation}
L(\theta)=\sum_{i}^{m} \log \left( \sum_{j}^{k} W_{i,j} \left( \frac{p(x_{i},y_{i}=j|\theta)}{W_{i,j}} \right) \right)
\end{equation}
According to Jensen inequality\footnote{For every concave function $f$, $f(\mathbb{E}x) \geq \mathbb{E} f(x)$} and since $\sum_{j}^{k}W_{i,j}=1$:
\begin{equation}
L(\theta) \geq \sum_{i}^{m} \sum_{j}^{k} W_{i,j}  \log \left( \frac{p(x_{i},y_{i}=j|\theta)}{W_{i,j}} \right)=Q(W,\theta)
\end{equation}
\newtheorem{claim}{claim}
\begin{claim}
If $W_{i,j}^{(t)}=p\left(y_{i}=j|x_{i},\theta^{(t)}\right)$ then $L(\theta^{(t)})=Q(W^{(t)},\theta^{(t)})$.
\end{claim}
\begin{proof}
\begin{align}
\begin{aligned}
Q\left( p(y_{i}=j|x_{i}, \theta^{(t)}), \theta^{(t)} \right) 
&= \sum_{i,j}p(y_{i}=j|x_{i},\theta^{(t)}) \log \frac{p(y_{i}=j|x_{i},\theta^{(t)}) p(x_{i}|\theta^{(t)})}{ p(y_{i}=j|x_{i},\theta^{(t)} ) }\\
&=\sum_{i}\underbrace{\sum_{j}p(y_{i}=j|x_{i},\theta^{(t)})}_{=1} \log p(x_{i}|\theta^{(t)})\\
&=\sum_{i}\log p(x_{i} | \theta^{(t)}) = L(\theta ^{t} )
\end{aligned}
\end{align}
\end{proof}

Hence,


 $\left[ p(y_{i}=j | x_{i} \theta^{(t+1)}  \right]_{i,j} = \underset{W}{\text{argmax}} Q(W, \theta^{(t)})$
 
 $\Rightarrow \theta^{(t+1)}=\underset{\theta}{\text{argmax}} Q\left( p(y|x,\theta^{(t)}),\theta \right)$

and since $Q\left( p(y|x,\theta^{t},\theta) \right) = \sum_{i,j} p(y_{i}=j|x_{i},\theta^{(t)} ) \ln \frac{ p(x_{i}|y_{i}=j,\theta) p(y_{i}=j|\theta)}{ \underbrace{p(y_{i}=j | x_{i},\theta^{(t)})}_{\text{does not depend on } \theta}} $ we get:


 $\Rightarrow \theta^{(t+1)}=\underset{\theta}{\text{argmax}} \sum_{i,j} W_{i,j}^{(t)} \ln \lambda_{j} p(x_{i}|y_{i}=j,\theta) $ 
 
 where $\lambda_{j}=p(y=j), \sum_{j}{\lambda_{j}}=1$


So far, we have presented the general framework for the EM algorithm, without using a specific model with parameter vector $\theta$. We will now use Mixture of Gaussians (MoG) as an example for a possible distribution that can be fit using EM. 


\subsubsection{Mixture of Gaussians}

The probability density function of a general multivariate Gaussian distribution is:
$N_{x_{i}}\left[ c_{j} ; E_{j}  \right] = \frac{1}{(2\pi)^{M/2}\left| E_{j} \right|^{1/2}}$ where $m$ is the dimension of the data, $\mu \in \mathbb{R}^{m}$ is the location, $E\in \mathbb{R}^{m \times m}$ is the covariance matrix (a positive definite matrix). 

Let the probability of $x_{i}$ to belong the $j$-th cluster given the Gaussian distribution parameters $\theta=(c_{1},...,c_{k},E_{1},...,E_{k},\lambda_{1},...,\lambda_{k})$ be 
 \begin{equation}
 p(x_{i} | y_{i}=j,\theta)  =  N_{x_{i}}\left[ c_{j} ; E_{j}  \right] = \frac{1}{(2\pi)^{M/2}\left| E_{j} \right|^{1/2}}
 e^{-\frac{1}{2}(x_{i}-c_{j})E^{-1}(x_{i}-c_{j})}
 \end{equation}

Then
\begin{align}
\begin{aligned}
\theta^{(t+1)} 
&=\underset{\theta}{\text{argmax}} Q\left( p(y|x,\theta^{(t)}),\theta \right)
\\
&= \underset{\theta}{\text{argmax}}
\left(
  \sum_{i,j}W_{i,j}^{(t)}  \left( \ln \lambda_{j} + \ln N_{x_{i}}\left[ c_{j} , E_{j}  \right]  \right)
\right) \text{s.t. } \sum_{j}\lambda=1
\\
&= \underset{\theta}{\text{argmax}}
\left(
  \sum_{i,j}W_{i,j}^{(t)}  \left( \ln \lambda_{j} -\ln \frac{1}{(2\pi)^{M/2}\left| E_{j} \right|^{1/2}}  e^{-\frac{1}{2}(x_{i}-c_{j})E^{-1}(x_{i}-c_{j})} \right)
\right) \text{s.t. } \sum_{j}\lambda=1
\\
&= \underset{\theta}{\text{argmax}}
\left(
  \sum_{i,j}W_{i,j}^{(t)}  \left( \ln \lambda_{j} -\frac{1}{2}\ln|E_{j}| +  \frac{1}{2}(x_{i}-c_{j})^{T}E^{-1}(x_{i}-c_{j})  \right)
\right) \text{s.t. } \sum_{j}\lambda=1
\end{aligned}
\end{align}

Now, to find a value of $Q$, we derive by each of the model variables ($W$ is fixed from the previous iteration).

%%% Deriving by \c_{j}

\textbf{Deriving by} $c_{j}$:

\begin{align}
\begin{aligned}
\frac{\partial}{\partial c_{j}}Q(W^{(t+1)},\theta) 
 &=\frac{\partial}{\partial c_{j}} \left[ -\frac{1}{2}\sum_{i}W_{i,j}(x_{i}-c_{j})^{T}E^{-1}_{j}(x_{i}-c_{j}) \right] \\
 &=\frac{1}{2}\sum_{i}W_{i,j}E^{-1}(x_{i}-c_{j}) \\
 &=\frac{1}{2}E^{-1}\sum_{i}W_{i,j}(x_{i}-c_{j}) = 0 \\
\Rightarrow & \sum_{i}W_{i,j}(x_{i}-c_{j}) = 0 \cdot 2 E = 0 \\
\Rightarrow & \sum_{i}W_{i,j}x_{i}- \sum_{i}W_{i,j}c_{j} = 0 \\
\Rightarrow & c_{j} = \frac{1}{\sum_{i}W_{i,j}} \sum_{i}W_{i,j}x_{i} \\
\end{aligned}
\end{align}


%%% Deriving by \lambda_{j}

\textbf{Deriving by} $\lambda_{j}$:

\begin{align}
\begin{aligned}
\frac{\partial}{\partial \lambda_{j}}\left[Q(W^{(t+1)},\theta) + {\delta}\cdot  \sum_{j}\lambda_{j}-1 \right]
&= 
\frac{\partial}{\partial \lambda_{j}} 
\left(
  \sum_{i,j}W_{i,j}^{(t)}  \left( \ln \lambda_{j} -\frac{1}{2}\ln|E_{j}| +  \frac{1}{2}(x_{i}-c_{j})^{T}E^{-1}(x_{i}-c_{j})\right) -\delta\lambda_{j}
\right) \\
&=\sum_{i} W^{(t)}_{i,j} \frac{1}{\lambda_{j}} - \delta\\
&=\frac{1}{\lambda_{j}}\sum_{i} W^{(t)}_{i,j} - \delta\\
\Rightarrow & \frac{1}{\lambda_{j}}\sum_{i} W^{(t)}_{i,j} = \delta\\
\Rightarrow & \frac{1}{\delta}\sum_{i} W^{(t)}_{i,j} = \lambda_{j}\\
\end{aligned}
\end{align}

Now, since $\sum_{j}\lambda_{j}=1$ we get $\frac{1}{\delta} \sum_{i}\underbrace{\sum_{j} W^{(t)}_{i,j}}_{=1} = \frac{1}{\delta}m\cdot 1 = 1 \Rightarrow \delta=m$.

Therefore:
$\lambda_{j}=\frac{1}{m}\sum_{i}W_{i,j}^{(t)}$.

%%% Deriving by E

\textbf{Deriving by} $E_{j}$:

TBD

\subsubsection{Special case, soft K-Means: E=I}

If $E=I$ then, $p(x_{i}|y_{i}=j,\theta)=\frac{1}{2\pi^{m/2}}e^{-\frac{1}{2}\|x_{i}-c_{j}\|^{2}}$ and

$\theta^{(t+1)}= \underset{\theta}{\text{argmax}} \left[ 
\sum_{i,j}W_{i,j}^{(t)}\ln \lambda_{j}-\frac{1}{2}\sum_{i,j}W_{i,j}^{(t)}\|x_{i}-c_{j}\|^{2} 
\right]$

$\lambda_{j}:=p(y_{i}=j$ is prior, assume uniform priors $\lambda_{j}=\frac{1}{k}$.

Now we get a soft version of k-means:
$\theta^{(t+1)}=\underset{{c_{1},...,c_{k}}}{\text{argmin}}\frac{1}{2}\sum_{i,j}W_{i,j}^{(t)}\| x_{i}-c_{j} \|^{2}$


%%%%%%
\section{Affinity-based Clustering (Graph-based methods)}

$x_{1},...,x_{m}$ input points.
We are given an affinity matrix $K\in \mathbb{R}^{m\times m}$ where $K_{r,s}=e^{-\frac{\|x_{r}-x_{s}\|}{2\pi^{2}}}$.
Note that $0 \leq K_{r,s} \leq 1$ and $K_{r,s}$ is higher when $x_{r}$ and $x_{s}$ are similar (i.e. likely to be in the same cluster).
$K$ defines a graph $G(V,E)$ whose vertices $V=\{x_{1},...,x_{m}\}$ represent points and whose edges $E$ have weights as define by $K$: $E(r,s)=K_{r,s}$.

In EM, we defined $W\in R^{m\times k}$ with $W_{i,j}=p(y_{i}=j|x_{i})$, ``soft assignment'' and in k-means $W_{i,j}$ is a ``hard'' assignment.

Here again, $W$ is the output, and the input is $K$ (rather than the points themselves).
What can we say about $W$ as a function $f$ of $K$?

Ideally, $W=\argmax_{W} f(W,K)$ subject to constraints on $W$.

Let's start by specifying the constraints on $W$:

\begin{enumerate}
\item $W_{i,j} \geq 0$
\item $\sum_{j}W_{i,j} = 1$ (i.e. $W\cdot \overrightarrow{1} = \overrightarrow{1}$)
\item $W^{T}W=D$, where $D$ is a diagonal matrix (i.e. $i\neq j \Rightarrow W_{i}\neq W_{j} = 0$, so there is only one non-zero number per row).
\end{enumerate}

Due to (2) and (3) we get that $W_{i,j}\in \{0,1 \}$.

Now we can view $K_{r,s}$ as a probability:
$K_{r,s}=p(x_{r},x_{s} \text{ belong to the same cluster})=\sum_{j}^{k}p(y_{r}=j,y_{s}=j|x_{r},x_{s})$.

Let's assume that $p(y_{r}=j,y_{s}=j|x_{r},x_{s})=p(y_{r}=j|x_{r})p(y_{s}=j|x_{s})=W_{rj}\cdot W_{sj}$ and get that $K_{r,s}=\sum_{j=1}^{k}W_{rj}\cdot W_{sj}$. This yields that we are looking for $W$ for which:
\begin{equation}
K=WW^{T} \left(\in R^{m\times m} \right)
\end{equation}

However, such $W$ that also satisfies the constraints specified above may not exist. Therefore, we aim to find the closest solution that also satisfies the constraints.

One way to express similarity between $K$ and $WW^{T}$ is by inner product:

$W=\argmax_{W} \sum_{r,s}K_{r,s}(WW^{T})_{r,s}=\argmax_{W} \text{tr}(KWW^{T})=
$\footnote{the last equality follows from the fact that the trace is invariant under cyclic permutations, i.e. $\text{tr}(ABC)=\text{tr}(CBA)$}
$\text{tr}(W^{T}KW)$.

Putting all together yields the following optimization problem:

\begin{equation*}
\begin{aligned}
& \argmax_{W} & & \text{tr}(W^{T}KW) \\
& \text{subject to}
& & W \geq  0, \\
&&& W\cdot \overrightarrow{1} = \overrightarrow{1}, \\
&&& W^{T}W=D.
\end{aligned}
\end{equation*}


\newtheorem{claim2}{claim2}
\begin{claim}
If $k=2$ (i.e. two clusters are assumed), the problem is min-cut.
\end{claim}

\begin{proof}

$\textbf{tr}(W^{T}KW) = \sum_{r,s}K_{r,s}(WW^{T})_{r,s}=\sum_{r,s}K_{r,s}\sum_{j=1}^{2}W_{r,j}W_{s,j}$\footnote{
$\sum_{j=1}^{2}W_{r,j}W_{s,j}$ is $1$ if $x_{r}$ and $x_{s}$ are in the same cluster and $0$ otherwise.}

$=\sum_{r,s}K_{r,s} W_{r,1}W_{s,1}  + \sum_{r,s}K_{r,s} W_{r,2}W_{s,2} $

$=\sum_{r,s}K_{r,s} + \sum_{r,s}K_{r,s}$

Hence

$W=\argmax_{W} \sum_{y_{r}=y_{s}=1}K_{r,s} + \sum_{y_{r}=y_{s}=2}K_{r,s}=$

$=\argmin_{W} \sum_{y_{r}=1,y_{s}=2}K_{r,s} = \text{cut in }G$

\end{proof}

The problem with min-cut is that it often generates imbalanced clusters, because the cut increases with the number of crossing edges so cutting out a single vertex may have a better cut value.

Therefore, ``balancing'' constraints on $W$ have been developed.

\subsection{Adding ``balancing'' constraints to $W$}

Requiring $\underbrace{W^{T}\overrightarrow{1}}_{\text{Number of points per cluster}}=\frac{m}{k} \overrightarrow{1}$ (i.e. the number of points in each cluster should be the same) is ideal, but may be too strong. 
Hence we want to get something softer:

$W\overrightarrow{1}=\overrightarrow{1}$ and $W^{T}\overrightarrow{1}=\frac{m}{k} \overrightarrow{1}$  
$\Rightarrow WW^{T} \overrightarrow{1} = \frac{m}{k}\overrightarrow{1}$ (but not vice-verse).

This is almost a doubly-stochastic matrix (whose sums of columns and sums of rows are all equal to 1), except that we get $\frac{m}{k}$.

Observe that $W^{T}W=D,    W^{T}\overrightarrow{1}=\frac{m}{k}\overrightarrow{1} \Rightarrow D=$
\footnote{$W^{T}W=D \Rightarrow W^{T}W\overrightarrow{1}=D\overrightarrow{1} \Rightarrow W^{T}\overrightarrow{1}=D\overrightarrow{1}\Rightarrow D=\frac{m}{k}I$}
$\frac{m}{k}I$.
\begin{proof}
$\frac{m}{k}\overrightarrow{1}=W^{T}\overrightarrow{1}=W^{T}\underbrace{W\overrightarrow{1}}_{\overrightarrow{1}} = D\overrightarrow{1}\Rightarrow D=\frac{m}{k}I$
\end{proof}
Therefore we can rewrite the problem of finding the assignments $W$ as:
\begin{equation*}
\begin{aligned}
W=& \argmax_{W} 
& & tr(W^{T}KW) \\
& \text{subject to}
&& W \geq 0 \\
&& & WW^{T}\overrightarrow{1}=\frac{m}{k}\overrightarrow{1}, \\
&&& W^{T}W=\frac{m}{k}I
\end{aligned}
\end{equation*}

But since, $\frac{m}{k}$ is a scale factor we can remove it and still get the same $W$:
\begin{equation*}
\begin{aligned}
W=& \argmax_{W} 
& & tr(W^{T}KW) \\
& \text{subject to}
&& W \geq 0 \\
& & & WW^{T}\overrightarrow{1}=\overrightarrow{1}, \\
&&& W^{T}W=I
\end{aligned}
\end{equation*}

Now the required $W$ is ``doubly stochastic''.

Because we want $WW^{T}$ to be similar to $K$ and also doubly stochastic, we can replace $K$, with a doubly stochastic matrix $K'$ that is similar to $K$, and then the optimal $W$ will also be doubly stochastic (because it's similar to $K'$ which is double stochastic). Thus, we can remove the last constraint:

\begin{equation*}
\begin{aligned}
W=& \argmax_{W} 
& & tr(W^{T}K'W) \\
& \text{subject to}
&& W \geq 0 \\
%& & WW^{T}\overrightarrow{1}=\overrightarrow{1}, \\
&&& W^{T}W=I
\end{aligned}
\end{equation*}

where $k$ is defined as follows:
\begin{equation*}
\begin{aligned}
K = & \argmax_{K'} 
& & K'=\argmin_{K'} = dist(K-K') \\
& \text{subject to}
&& K^{T}K\overrightarrow{1}=\overrightarrow{1}
\end{aligned}
\end{equation*}

Since solving with the first (positivity) constraint is hard, we will remove it to get an easier problem with a closed-form solution, which is the $k$ leading eigenvectors of $K'$.

\subsubsection{Normalized-cuts}
In \textbf{normalized-cuts} $K'=D^{-\frac{1}{2}}KD^{-\frac{1}{2}}$ where $D=diag(K\overrightarrow{1})$ (a diagonal matrix with the sums of rows of $K$ on the diagonal). $K'$ is the closest doubly stochastic matrix in relative entropy to $K$.
TBD: Proof can be find in the hand-written notes.
This is the most popular clustering algorithm that tries to fix the problem of min-cut.

\subsubsection{Ratio-cuts}
In \textbf{ratio-cuts} $K'=K-D+I$, $K'$ is the closet doubly stochastic matrix in relative $L_{1}$ norm, where $D=diag(K\overrightarrow{1})$ as in normalized cuts.


\newtheorem{claim-ratio}{claim}
\begin{claim}
$K'=K-D+I$, $K'$ is doubly stochastic and 
\begin{equation*}
\begin{aligned}
K = & \argmax_{K'} 
& & K'=\argmin_{F}  \|K-F\|_{1} \\
& \text{subject to}
&& F\overrightarrow{1}=\overrightarrow{1} \\
&&& F=F^{T}
\end{aligned}
\end{equation*}

\end{claim}

\begin{proof}
Observe that $\|A\|_{1} \geq \|A\overrightarrow{1}\|_{1}$ for every matrix $A$.

Therefore for every $F$ that satisfies the constraints:
\begin{equation}
\| K - F \|_{1} \geq \| (K-F) \overrightarrow{1} \|_{1}=\| D\overrightarrow{1}-F\overrightarrow{1} \|_{1}=\| D-\overrightarrow{1} \|_{1}=\| D-I \|_{1}
\end{equation}
Now, substitute $F=K-D+I$ and get $\| K-(K-D+I)\|_{1} = \| D-I \|_{1}$

\end{proof}

\subsubsection{Relation to K-Means}

\newtheorem{claim3}{claim}
\begin{claim}
$W=\argmax_{W}tr(W^{T}KW)$ subject to $W_{i,j}=1[y_{i}=j]\frac{1}{\sqrt{n_{j}}}$ is the k-means solution for $K_{i,j}=x_{i}^{T}x_{j}$
\end{claim}

\begin{proof}
substitute $c_{j}=\frac{1}{\sum_{i}W_{i,j}}\sum_{i,j}W_{i,j}x_{i}$ in $\min\sum_{i,j}W_{i,j}\|x_{i}-c_{j}\|^{2}$ and note that $n_{j}=\sum_{i}W_{i,j}$.
\end{proof}




%%% Ratio-cuts original derivation 

\subsection{Ratio Cuts: Original Derivation (2-clusters only)}
Ratio-cut can be viewed as an approximation for min-cut.

Min-Cut: $y = \argmax_{y\in \{ +- 1 \}^{m}} $.

Ratio-Cut: 
\begin{equation*}
\begin{aligned}
z = & \argmin_{z\in \mathbb{R}^{m}} 
& & \frac{1}{2}  \sum_{r,s}(z_{r}-z_{s})^{2}K_{r,s} \\
& \text{subject to}
&& z^{t}z=1 \\
%&&& F=F^{T}
\end{aligned}
\end{equation*}

(i.e. $z_{r}$ is close to $z_{s}$ if $x_{r}$ and $x_{s}$ should be in the same cluster).

\newtheorem{claim5}{claim}
\begin{claim}
$\frac{1}{2}\sum_{r,s}(z_{r}-z_{s})^{2}K_{r,s} = z^{T}(D-K)z$ (D-K is a laplacian matrix)
\end{claim}
\begin{proof}

$\sum_{r,s}(z_{r}-z_{s})^{2}K_{r,s}$

$=\sum_{r,s}z_{r}^{2}K_{r,s}-2\sum_{r,s}z_{r}z_{s}K_{r,s}+\sum_{r,s}z_{s}^{2}K_{r,s}$

$=2\sum_{r,s}z_{r}^{2}K_{r,s}-2\sum_{r,s}z_{r}z_{s}K_{r,s}$

$=2(z^{T}Dz-z^{T}Kz)$
\end{proof}

Our objective is this:
\begin{equation*}
\begin{aligned}
z = & \argmin_{z\in \mathbb{R}^{m}} 
& & z^{T}(D-K)z \\
& \text{subject to}
&& z^{t}z=1
\end{aligned}
\end{equation*}

\newtheorem{claim6}{claim}
\begin{claim}
The solution is the smallest eigenvector of $D-K$.
\end{claim}
\begin{proof}
$v=[1,...,1]^{T}$ is an eigenvalue of $D-K$ with eigenvalue $\lambda=0$.
Since $D-K$ is PSD, $\lambda=0$ is its smallest eigenvalue, but $v$ is not meaningful for clustering.
Therefore, we choose $z$ as the second smallest eigenvector of $D-K$.
Since $z^{T}v=0$ (because $z$ belongs to another eigen-space than $v$), $z$ must have both positive and negative entries, so can cluster according the entries' signs.

$(v,\lambda)$ is a spectral pair of $D-K \Leftrightarrow (v,1-\lambda)$ is a pair of $I-(D-K)$. Therefore we conclude that $z$ is the second largest eigenvector of $I-(D-K)=K-D+I$.
\end{proof}

\subsection{Normalized Cuts: Original Derivation}
Denote the assignment into $k$ clusters by $y_{1},...,y_{m}\in [k]$, and the clusters themselves as $\psi,_{1},...,\psi_{k}$. We require that the clusters will be disjoint and each point belongs exactly to one cluster.

Define:
\begin{itemize}
\item $\| \psi_{j} \| = \sum_{y_{r}=y_{s}=j} K_{r,s}$ (i.e. sum of internal weights in $\psi_{j}$).
\item $cut(\psi_{i}, \psi_{j}) = \sum_{y_{r}=i \neq y_{s}=j} K_{r,s}$ (i.e. sum of weights crossing between different clusters)
\end{itemize}

We would like to partition the vertices of the graph such that $\{ \| \psi_{j} \| \}_{j=1}^{k}$ will be high and $\{ cut( \psi_{i}, \psi_{j} ) \}_{i\neq j}$ will be low.

Consider the maximization of $E_{k}(y)=\sum^{k}_{j=1}\frac{\| \psi_{j} \|}{ \sum_{s=1}^{m} \sum_{y_{r}=j} K_{r,s} }$
$=\sum_{j=1}^{k}\frac{\| \psi_{j} \|}{ \psi_{j} + \sum_{i\neq j} cut(\psi_{i}, \psi_{j} }$.

For example, in the two-class case: $E_{2}(y)\leq 2$. If $cut(\psi_{1},\psi_{2})=0$ then $E_{2}(y)=2$, regardless of the cluster sizes. However, if $cut(\psi_{1},\psi_{2}) > 0$ then $E_{2}(y)$ is larger for clusters with similar sizes.

For an assignment $y_{1},...,y_{m}$ we define: \\
$W_{j}=\frac{1}{(\sum_{s=1}^{m}\sum_{r:y_{r}=j}K_{r,s})^{0.5}} \left[ 1|[y_{1}=j],...,1|[y_{m}=j]  \right]^{T}\in \mathbb{R}^{m}$ \\
$\Rightarrow W=\left[ w_{i}, ..., w_{k}  \right] \in \mathbb{R}^{m\times k}$

For each $j \in [k]$:\\
$W_{j}KW_{j} = \frac{\sum_{r,s}K_{r,s} \left[ 1|[y_{r}=j] \cdot 1|[y_{s}=j]\right] }{\sum_{s=1}^{m}\sum_{r:y_{r}=j}K_{r,s}}
=\frac{\| \psi_{j} \|}{ \sum_{s=1}^{m}\sum_{r:y_{r}=j} K_{r,s}}$.\\
This implies that $trace(W^{T}KW) = E_{k}(y)$.

Now redefine $W$ as $W=D^{1/2}W$.\\
Then: $W^{T}W=I$ and $trace(W^{T}\underbrace{D^{-1/2}KD^{-1/2}}_{K'}W)=E(y)$.

We optimize over $W$, leaving only the constraint $W^{T}W=I$ (relaxation, $W$ does not necessarily originate from a cluster assignment $y_{1},...,y_{m}$):
\begin{equation*}
\begin{aligned}
W = & \argmax_{W} 
& & trace(W^{T}K'W) \\
& \text{subject to}
&& W^{T}W=I
\end{aligned}
\end{equation*}
Hence, the columns of $W$ are the $k$ leading eigenvectors of $K'$.

\newtheorem{claim7}{claim}
\begin{claim}
$(v=D^{1/2}\overrightarrow{1}, \lambda=1)$ is the largest spectral pair of $K'$.
\end{claim}
\begin{proof}
This is a spectral pair because:\\
$K'D^{1/2}\overrightarrow{1}=D^{-1/2}KD^{-1/2}D\overrightarrow{1}=D^{-1/2}K\overrightarrow{1}=D^{-1/2}D\overrightarrow{1}=D^{1/2}1$.\\
1 is the maximal eigenvalue of $K'$ because:\\
$D-K \geq 0 \Rightarrow D^{-1/2}(D-K)D^{-1/2} \geq 0$
$\Rightarrow I-D^{-1/2}KD^{-1/2} \geq 0 \Rightarrow \lambda_{i}(K')\leq 1$
\end{proof}

In a 2-class setting (k=2), it is customary to take the second largest eigenvector of $K'$ and cluster according to its entries' signed (it will have both positive and negative entries as it is orthogonal to $v=D^{1/2}\overrightarrow{1}\geq 0$ because they belong to different eigen spaces.
