%!TEX root = cv-notes.tex
\chapter{Machine Learning Basics}

\section{Fundamentals of Statistical Learning}

Let $X$ be a domain set, for example $\mathbb{R}^{d}$, and let $Y$ be a domain set, for example $\{0,1\}$, $\{1,...,k\}$, $\mathbb{R}$, etc..

$D$ is an unknown distribution over $X\times Y$

$S=\{(x_{},y_{i})\}_{i=1}^{m}$ is a training set, and we assume that $S\sim D^{m}$ (sampled i.i.d.)

Goal: ``learn'' $h:X \rightarrow Y$ from input $S$.

Loss function:

Let $l(h;(x,y))$ be the ``penalty'' of wrong predictions.
For example, $l(h;(x,y))=1[h(x)\neq y]$ is called $0/1$ loss.
$l(h;(x,y))=(h(x)-y)^{2}$ is square loss.
Later, we will discuss about loss functions in more details.

Define $L_{0}(h)=E_{(x,y)\sim D} \left [ l(h;(x,y)) \right]$ the expectation of prediction error.

Since we do not know $D$, we cannot work with $L_{D}(h)$, 
but we can work with $L_{S}(h)=\frac{1}{m} \sum_{i=1}^{m}l(h;(x_{i},y_{i}))$.

\subsection{Expected Risk Min (ERM) principle}

The natural thing to do:

$h*=\argmin_{h} L_{s}(h)$

The motivation: law of large numbers: empirical distribution converges to the try distribution with the number of samples.

The problem with ERM is ``over-fitting'', i.e. $L_{s}=0$ yet $L_{D}(h)$ is large.

\subsubsection{ERM with inclusive basis}

The idea is to look for conditions for which $L_{D}(h) < \epsilon$ using ERM.

Approach: Restrict the search space of predictions $h:X\rightarrow Y$.
The restriction is done before the data $S$ is observed.
Let $H$ be the restricted class of predictions.

$ERM_{H}(S)=\argmin_{h\in H} L_{S}(h)$

We are biasing the solution towards a particular set H, called ``inductive bias''.

$H$ is selected based on some prior knowledge about the problem.

\subsubsection{$(\epsilon, \lambda)$ -learning (PAC: Probably approx correct)}

$L_{D}(ERM_{H}(S))$ is a random variable because $S$ is picked by a random process.

We cannot be certain about the value of $L_{D}(ERM_{H}(S))$.

``Probably'': Controlled by $\delta > 0$. ``Approx'' $L_{D}(ERM_{H}(S)) \leq \epsilon$.

The model:

If $m > m_{H}(\epsilon, \delta)$ then with probability $\geq 1-\delta$ we will have $L_{D}(ERM_{H}(S)) \leq \epsilon$.

Claim:

If $H$ is finite, then if $m \geq \frac{\log \left( \frac{|H|}{\delta} \right)}{\epsilon}$ then $L_{D}(h^*)\leq$ with probability $\geq 1-\delta$.

In case that $\min_{h\in H} L_{s}(h) > 0)$ called ``agnostic'' then $m \geq \frac{\log \left( \frac{|H|}{\delta} \right)}{\epsilon^{2}}$.

The outcome is, if $H$ is a proper subset of all functions $X\rightarrow Y$, ``dim''$(H) < \alpha$.

Goal: $\argmin_{h \in H} L_{D} (h)$.

Instead we do $\argmin_{h\in H} L_{S}(h)$
and if $m$ is largee enough the almost always there will be no over-fitting.

In general ``dim''$(H)$ is replaced by VC-dim$(H)$ a combinatorial measure of complexity of the class H, then:
$m_{H}(\epsilon, \lambda) = \Theta\left(  \frac{d+\log(\frac{1}{\delta})}{\epsilon^{2}}  \right) \Rightarrow \epsilon \sim \sqrt \frac{d + \log(1/\delta)}{m}$



\subsection{Bias-Complexity Tradeoff}

So far, by restricting $H$ to a finite ``dim'' class, then it is possible to avoid over-fitting provided we have enough examples.

$L_{D}(ERM_{H}(S)) = \min_{h\in H} L_{D}(h) + \left(  L_{D}(ERM_{H}(S)) - \min_{h\in H}L_{D}(h)  \right)$

$\Rightarrow L_{D}(h_{S}^{*}) = L_{D}(h_{D}^{*}) + \left[ L_{D}(h_{S}^{*}) - L_{D}(h_{D}^{*}) \right]$

When the dimension of $H$ is high, the approximation error is low, but the estimation error is also high.
When the $m$ is high, the approximation error is high, but the estimation error is low.
We care about the estimation error, but we also want $m$ to be high (easier to achieve a smaller training set) so we would like to use a low dimensional $H$ that can ``explain'' the training set well enough.

We use a labeled test set to estimate the estimation error.
When we have over-fitting, we do not improve the error on the test set when increasing $m$.
When the model is constrained enough, we should see a decreased error on the test set when the approximation error is decreased.

Therefore, we use an hierarchical family of hypothesis functions and choose the most rich $H$ before the error on the test set starts to increase.

Before we explain how to construct such hypothesis hierarchy, we will describe loss functions in more detail.

\subsection{Linear Predictors for 2-class}

$h(x)=sign(<w,x>+\theta)$ where $h:X\rightarrow \{ -1, 1 \}$

We can remove the offset $\theta$ by adding '1' as the $d+1$ entry of $x$, so we will write
$h(x)=sign(<w,x>)$:

$H=\{ sign(<w,x>) : w \in \mathbb{R}^{d} \}$ class of homogenous half space. Each hypothesis forms a hyperplane perpendicular to $w$.

(TBD: See illustration in the hand-written notes)

\begin{itemize}
\item 0/1 loss
\item  hinge-loss (surrogate loss): $l_{h}(<w,x_{i}>;y_{i})=max(0, 1-y_{i}<w,x_{i}>)$. This is an upper bounds to 0/1-loss. Prediction is correct (i.e. zero penalty) if $y<w,x> \geq 1$ (instead of $\geq 0$). Also $w$ and $\lambda w$ are different classifiers (different loss). Prediction is ``correct'' if $y<w,x>$ is positive with margin of 1.
Goal: find a smooth upper bound of 0/1-loss.
1) Use ``soft-max'' to upper-bound hinge-loss: $max_{i}\{f(x_{i})\} \leq \ln( \sum_{x_{i} e^{f(x_{i})} } ) \\ \Rightarrow \max(0, 1-z) \leq \ln (1+e^{1-z})$

\item Exp-loss (used in Adaboost) $e^{-y<w,x>}$ bounds 0/1 loss

\item log-loss: $\log_{2}(1+ e^{-y<w,x>})$, used in max-likelihood estimation.
Instead of $y\in \{0,1\}$ define: $h(x) = \sigma (<w,x>)=\frac{1}{1+e^{-<w,x>}} := p(y=1 | x)$

\item square loss: $l_{2}(<w,x_{i}>; y_{i}) = (<w,x_{i}> - y_{i})^{2}$

\end{itemize}

\subsection{Creating a hierarchy $H_{1} \subset H_{2} \subset ...$  through regularization}

$w = \argmin_{w} L_{s(w)}$ subject to: $\|w\|_{2}\leq B$ an equivalent form $\argmin L_{s}(w)+\lambda \|w\|_{2}$.

$\lambda$ and $B$ are related (when $B$ is up, $lambda$ is down).
This defines a hierarchy of hypothesis spaces: $H_{B}=\{ w: \|w\|_{2 \leq B} \}$


\chapter{Visual Recognition}

\section{Visual Recognition from a Bounding Box}

Binary class goal: example, detect a face, a car a pedestrian in an image.

Questions:
\begin{enumerate}
\item Given a boundary box which may contain an object, how to classify?
	\begin{enumerate}
	\item What measurements to make?
	\item How do we represent the measurement? A single vector $x \in \mathbb{R}^{d}$ or multiple vectors (corresponding to ``parts'' of the object), etc.
	\item How to account for ``non-tigid'' variability (many body parts for example)?
	\end{enumerate}
\item How do we select the boundary box to begin with? The number of possibilities cover: location, scale, aspect ratio. Could be impractical to attempt to classify all possible bounding boxes.
\end{enumerate}

We will start by addressing question 1.

\subsection{Measurements: HOG}

Histogram of gradients (HOG) is a modern method for generating a dense representation (as opposed to focusing on sparse interest points).

\subsubsection{The original HOG [Triggs and Dalal 2005]}:

Boundary box divided into ``cells'' of $8 \times 8$, and each group of $2\times 2$ cells is integrated into a block in a sliding fashion, so blocks overlap with each other.
A cell is represented by a 9-bin orientation histogram.
A block contain the concatenate vector of its 4 cells, i.e. a vector in $R^{36}$.
Each block is normalized, i.e. each cell contributes to multiple blocks.

A $64\times 128$ bounding box is represented by $7\times 15$ blocks (total of $36 \times 7 \times 15 = 3780$ measurement vector.

Learning: $\{(x_{i}, y_{i})\}^{n}_{i=1}$, $x_{i}\in \mathbb{R}^{d}$, $d=3780$, $y_{i}\in \{-1, 1\}$.

Use linear SVM:

$h_{w}(x)=sign(w^{t}x)$

$L_{w}(s) = \max(0, \frac{1}{m}\sum_{i=1}^{m}\left( 1-y_{i}w^{T}x_{i} \right))$

$w^{*}=\argmin_{w} L_{s}(w)+\lambda \|w\|^{2}$

Issues:
\begin{enumerate}
\item How to efficiently compute histograms? (see ``Integral Histogram'' CVPR 2005).
\item HOG features restricted to single scale: 105 blocks of size $16\times 16$. Thes would pick only high frequency details. Instead one should design the features with blocks of varying size: $12 \times 12$ up to $64 \times 128$ (blocks of the size of the entire box). Also, aspect ratio of blocks should vary $1:1,1:2,2:1$. Also degree of overlap between blocks should vary: $4,6,8$ pixels $\Rightarrow$ instead of 105 blocks we could get thousands of blocks. Too many blocks: only a subset of them really matters.\\
Approach: Use framework $\min_{w}L_{s}(w)$ subject to: $\|w\|_{0}<T$ to select blocks.
\begin{itemize}
\item $d=$ number of blocks.
\item $h_{i}(x)=sign(w_{i}^{T}x)$ SVM for $i$-th block (trained offline)
\item $H(x)=sign\left( \sum^{d}_{j=1}\alpha_{j} h_{j}(x) \right)$ our classifier
\item $L_{S}(\alpha)=\frac{1}{m}\sum^{m}_{i=1}e^{-y_{i}H(x_{i}}$
\end{itemize}
See: [Zhu, Avidan, ... CVPR 2006]
\item Consider a complex shape like a pedestrian: The body parts can change relative position. How to account for it?
\end{enumerate}

\subsection{Latent-SVM [Felzenszwalb et al., PAMI 2010]}

We want to allow these parts to change their relative positions.

Model: $n$ pre-defined ``parts'' represented by filters: ($F_{0}, P_{1}, P_{2}$) where $F_{0}$ weight of the entire bounding box and $P_{i}=(F_{i}, V_{i}\in [-0.5,0.5]^{2}, d_{i}\in R^{4}$).

Object Hypothesis: $z=(P_{1},...,P_{n})$ a set of parts and their position.

$z$ is the model and we want to give it a score. Each $z$ corresponds to a set of template locations.

$score(z) = F \dot x + \sum_{j=1}^{n} F_{j} \dot x(p_{i}) - \sum_{j=1}^{n} d_{j}^{T}\theta(dx_{j} dy_{j})=w^{T}\phi(x,z)$\\
where $w=(F_{1},...,F_{n},d_{2},...,d_{n})$ and $\phi(x,z)=(x,x(P_{1}),...,X(P_{n}),-\phi(dx_{1},dy_{1}),...,-\phi(dx_{n},dy_{n}))$.

$max_{z}w^{T}\phi(x,z)$ in principle requires a large search space because each part can perturbable by $dx_{j},dy_{j}$. However, this can be evaluated efficiently using dynamic programming (details in the papers).

Classifier:

$h_{w}(x)=sign\left( \max_{z} w^{T}\phi (x,z)   \right)$

Loss function:

$L_{S}(w)=\frac{1}{m}\sum_{i=1}^{m}(1-y_{i}\max_{z}w^{T}\phi(x_{i}z))_{+}$

Latent-SVM:

$w^{*}=\argmin_{w}L_{S}(W)+\lambda \|w\|^{2}$

Problem: $L_{S}(w)$ is not convex, so the optimization is not trivial:

Let's separate the loss function to the positive and the negative examples:

$L_{S}(w)
=\underbrace{ \sum_{x,y=1}\max(0,1-\max_{Z}(W^{T}\phi(x,z))) }_\textrm{Concave}
+ \underbrace{ \sum_{x,y=-1}\max(0,1+\max_{z}(w^{T}\phi(x,z)))}_\textrm{Convex}$

The idea is to approximate the left term using an EM-like algorithm.
Let $w^{(t)}$ be the vector $w$ at the $t$-th iteration.
Define: $z(x,y=1)=\argmax_{z \in z(x)} {w^{(t)}}^{T}$, which can be found using dynamic programming.

$L^{(t)}=\sum_{x,y=-1} \max (0, 1+\max_{z\in z(x)} w^{T}\phi (x,z) +
\sum_{x,y=-1}\max(0,1+\max_{z}(w^{T}\phi(x,z^{(t)})))$

We have:

\begin{itemize}
\item $L^{(t)}$ is convex.
\item $L^{t}(w) \geq L(w)$
\item $L^{(t)}(w^{(t)}=L(w^{(t)})$
\end{itemize}

Let $w^{(t+1)}=\argmin_{w}L^{(t)}(w)+\lambda \|w\|^{2}$.
Then, $L(w^{(t+1)}) \leq L(w^{(t)})$

\section{Viola-Jones Classifier}

\begin{itemize}
\item Simple features (decision stamps)
\item Integral Image for efficient evaluation of features.
\item Adaboost for feature selection
\item Cascade
\end{itemize}

The simple features include three families:
\begin{itemize}
\item $\sum_{A}-\sum_{B}$
\item $\sum_{B}-\sum(A)-\sum_{C}$
\item $\sum_{A}+\sum{B}-\sum_{C}+\sum{D}$
\end{itemize}
where $A,B,C,D$ are adjacent rectangles whose sums can be efficiently computed using integral images.

\subsection{Decision stamps}

$h_{j}(x)$ is $1$ if $p_{j}f_{j}(x)<p_{j}\theta_{j}$ and -1 otherwise.
where $p_{j} \in \{-1,1\}$ and $f_{j}(x)=\sum_{A}-\sum_{B}$.

$\min_{\theta_{j},p_{j}}\frac{1}{m}\sum^{m}_{j=1}1[h_{j}\neq y_{j}] \alpha_{j}$

$H_{w}= sign\left( \sum_{j=1}^{180,000} w_{j}h_{j}(x) \right)$

$L_{S}(w)=\frac{1}{m}\sum_{i=1}^{m} e^{-y_{i}\sum_{j}w_{j}h_{j}(x)}$

$w^{*}=\argmin_{w} L_{S}(W)$ subject to: $\|w\|_{0} \neq T$ is the weight for each feature (after applying its $h$).

However, this is not enough, because the number of false positive is 1:14000 with 95\% true positive (per window, not per image).

\subsection{Cascade}

The idea is to train a growing number of classifiers (at each level) with zero false negative (so many non-face windows will be filtered out, but not all of them).

They used 150 levels.

We want to minimize the number of false negative with our loss function.

\subsubsection{Multivariate performance}

$h_{w}(x)=sign(f_{w}(x))$
$f_{w}(x)=w^{T}x=\sum_{i=1}^{m}\alpha_{i} K(x_{i},x)=w^{T}\Psi(x)$

where $\Psi=(K(x_{1},x),...,K(x_{m},x)$ kernel SVM.

This loss function
$L^{0/1}(w)=\frac{1}{m}\sum_{i=1}^{m}1[h_{w}(x_{i}\neq y_{i}]$
only measure the number of mistakes, but we are interested in counting the false negative vs. false positive.
So we need something else.

Assuming that we have $r$ windows after some filtering with a basic classifier (e.g. Viola-Jones).

$h_{w}(x)\in \{-1, 1\}^{r}=(z_{1},...,z_{r})$

$h_{w}(x)=sign(f_{w}(x_{1})),...,sign(f_{w}(x_{r}))$
$= \argmax_{z_{1},...,z_r \in {-1,1}^{r}} \left\{ \frac{1}{r} \sum_{j=1}^{r} z_{j} \underbrace{f_{w}(x_{j})}_{w^{T}x} \right\}$
$= \argmax_{z} w^{t}\phi(x_{i},z)$ where $\theta(x_{i},z)=\frac{1}{r} \sum_{j=1}^{r} z_{j}x_{j}$

So given an image with $r$ candidate windows, we train a classifier $h_{w}$ that gives us the optimal $z$.
When training, we can measure the performance of this classifier as a function of the the false negative, the false positive and so on, based on $a,b,c$ and $d$:
Define:
\begin{itemize}
\item $a$ = number of true positive
\item $b$ = number of false positive
\item $c$ = number of false negative
\item $d$ = number of true negative
\end{itemize}

$L_{S}(w)=\frac{1}{m}\sum_{i=1}^{m}C\left(h_{w}(x^{(i)} ), y^{(i)}\right)$

$L_{S}(w)=\frac{1}{m}\sum_{i=1}^{m} max_{z}\left\{ C(y,z)+w^{T}\phi (x,z) - w^{T}\phi (x,y) \right\}$

where $C$ is a function of $a,b,c$ and $d$.

Examples:
\begin{itemize}
\item 0/1 loss $b+c$
\item recall: $\frac{c}{a+c}$
\item precision = $\frac{b}{a+b}$
\end{itemize}

$l_{recall}(h_{w}(c),y) \leq l_{recall}(h_{w}(x),y)+\left( max_{z}w^{T}\phi(x,z)- w^{T}\phi(x,y) \right)$\\
$\leq \max_{z} \left\{ l_{reacall}(z,y) + w^{T}\phi(x,z)-w^{T}\phi(x,y) \right\}$\\
$L_{S}(w)=\frac{1}{m}\sum_{j=1}^{m}\max_{z}\left\{ l_{recall}(z,y^{(i)}) + w^{T}\phi(x^{(i)},z)-w^{T}\phi(x^{(i)},y^{(i)})  \right\}$

$w^{*}=\argmin_{w} L_{S}(w) + \frac{\lambda}{2}\|w\|^{2}$

We still have to scan $2^{r}$ vectors to find $\max_{z}$.

Sample: an image $(x^{(i)},y^{(i)})$ from $S$ (training set).

Solve: $z_{i}^{*} = \argmax_{z\in \{-1,1\}^{r}} \{ l_{recall}(z,y^{(i)})+w^{T}\phi(x^{(i)},z) \}$

Update: $w^{(t+1)}=(1-\frac{1}{t})w^{(t)}-\frac{1}{\lambda t} \left( \phi(x^{(i)},z_{i}^{*} \right) - \phi (x^{(i)}, y^{(i)})$

So how do we solve?
Search over $z$ can be replaced by search over $Y_{a,b}$.

Given $a$ and $b$, we know $c$ and $d$:
$c=|\{j:y_{j}=1\}|-a$, $d=|\{j:y_{j}=1\}|-b$
Once $C(y,z)$ is fixed, we need to maximize:
$\max_{z} w^{T}\phi (x^{(i)},z) = \max_{z} \sum_{j=1}^{r}z_{j}w^{T}x_{j}$

This can be done by:
\begin{itemize}
\item Set $z_{j}=1$ for the top $a$ positive examples ($y_{j}=1$).
\item Set $z_{j}=1$ for the top $a$ positive examples ($y_{j}=-1$).
\end{itemize}


